package cppcloud

import (
	"fmt"
	"strings"
	"testing"
)

func TestCliFun(t *testing.T) {
	app := MakeTCPCli("www.cppcloud.cn:4800", nil, 3)
	if nil == app {
		t.FailNow()
	}

	fmt.Println("cliApp:", app)
	mp := make(map[string]interface{})
	mp["hek"] = "body"
	mp["key"] = 1234
	mp["arr"] = []string{"first", "sec"}

	nbyte := app.sendMap(1, 1, mp)
	cmdid, seqid, msg, err := app.recv()

	fmt.Println("nbyte=", nbyte)
	fmt.Println("Rsp:", cmdid, seqid, msg, err)

}

func TestCheckConn(t *testing.T) {
	mp := make(map[string]interface{})
	mp["hek"] = "body"
	mp["svrid"] = 1234
	app := MakeTCPCli("www.cppcloud.cn:4800", mp, 3)

	fmt.Println(app)
}

func TestStringSplit(t *testing.T) {
	str := "/ho/vo/good"
	arr := strings.Split(str, "/")

	fmt.Println(arr)
}
